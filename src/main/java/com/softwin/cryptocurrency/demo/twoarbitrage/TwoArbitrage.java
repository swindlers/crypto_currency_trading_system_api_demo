package com.softwin.cryptocurrency.demo.twoarbitrage;
import lombok.*;

import com.softwin.cryptocurrency.demo.security.JwtCallCredential;
import com.softwin.grpc.common.Common;
import com.softwin.marketdata.gateway.service.MarketDataManageGrpc;
import com.softwin.marketdata.gateway.service.MarketGateway;
import com.softwin.order.reporting.service.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * Created by Huai weicheng on 2018/08/01.
 */


public class TwoArbitrage {
    @Data
    @AllArgsConstructor
    private class BidAsk1{

        private String exchange;

        private BigDecimal bid1Price;
        private BigDecimal bid1Quantity;

        private BigDecimal ask1Price;
        private BigDecimal ask1Quantity;

        private long timestamp;

        public boolean equals(Object obj) {
            if (obj instanceof BidAsk1) {
                BidAsk1 bidAsk1 = (BidAsk1) obj;
                return (exchange.equals(bidAsk1.exchange) && bid1Price.compareTo(bidAsk1.bid1Price) == 0
                        && bid1Quantity.compareTo(bidAsk1.bid1Quantity) == 0 &&  ask1Price.compareTo(bidAsk1.ask1Price) == 0
                        && ask1Quantity.compareTo(bidAsk1.ask1Quantity) == 0);
            }
            return super.equals(obj);
        }

        public int hashCode() {
            return exchange.hashCode() + bid1Price.toString().hashCode() + ask1Price.toString().hashCode() +
                    bid1Quantity.toString().hashCode() + ask1Quantity.toString().hashCode();
        }

    }

    @Data
    private class ArbitrageSymbol{
        private String baseExchange;
        private String quoteExchange;
        private BidAsk1 baseBidAsk1;
        private BidAsk1 quoteBidAsk1;

        public ArbitrageSymbol(String baseExchange, String quoteExchange) {
            this.baseExchange = baseExchange;
            this.quoteExchange = quoteExchange;
        }

        public BigDecimal getBaseQuoteSpread() {
            if (baseBidAsk1 != null && quoteBidAsk1 != null)
                return baseBidAsk1.getAsk1Price().subtract(quoteBidAsk1.getBid1Price());
            return BigDecimal.ZERO;
        }

        public BigDecimal getQuoteBaseSpread() {
            if (baseBidAsk1 != null && quoteBidAsk1 != null)
                return quoteBidAsk1.getAsk1Price().subtract(baseBidAsk1.getBid1Price());
            return BigDecimal.ZERO;
        }

        public void update(String exchange, BidAsk1 bidAsk1) {
            if (baseExchange.equals(exchange))
                baseBidAsk1 = bidAsk1;
            else if (quoteExchange.equals(exchange))
                quoteBidAsk1 = bidAsk1;
        }

        @Override
        public String toString() {
            return baseExchange + " ask1 - "  + quoteExchange + " bid1 = " +
                    getBaseQuoteSpread().setScale(5, RoundingMode.HALF_EVEN).toString() + "\t" +
                    quoteExchange + " ask1 - "  + baseExchange + " bid1 = " +
                    getQuoteBaseSpread().setScale(5, RoundingMode.HALF_EVEN).toString();
        }

    }

    private String host = "192.168.1.162"; //服务器地址
    private int mgport = 8991;
    private int tgport = 8993;
    private String username = "testTrader"; //交易员用户名
    private String password = "66666"; //交易员密码
    private String baseCurrency = "btc";
    private String quoteCurrency = "usdt";

    private String quantity = "0.0001"; //下单量
    private BigDecimal gap = new BigDecimal("5"); // ask1 与 bid1的差距达到一个gap后才触发交易

    private List<String> exchanges = Arrays.asList("huobi", "binance", "okex"); // 交易所
    private List<Common.Symbol> symbols = new ArrayList<>(); //交易币对
    private Map<String, Map<String, BigDecimal>> lastCurrencyMap = new HashMap<>(); //最新仓位
    private Map<String, BidAsk1> bidAsk1Map = new HashMap<>(); //最新bid1ask1
    private Map<String, ArbitrageSymbol> arbitrageSymbolMap = new HashMap<>(); //最新arbitrage symbol

    public List<List<String>> records = new ArrayList<>();


    private OrderReportingGrpc.OrderReportingStub tgStub; //交易网关
    private MarketDataManageGrpc.MarketDataManageStub mgStub; //行情网关
    private QueryServiceGrpc.QueryServiceStub queryStub; //查询网关
    private QueryServiceGrpc.QueryServiceBlockingStub queryBlockingStub ; //查询网关（阻塞）

    private Map<String, String> orderMap = new HashMap<>(); //等待成交的报单，key为方向(buy/sell)，value为orderId

    public static void main(String[] args)  throws InterruptedException {
        TwoArbitrage ta = new TwoArbitrage();
        Thread.sleep(1000 * 60 * 30);
        Array2CSV(ta.records, "data.csv");
    }

    /**
     * 初始化均值计算
     * 初始化三个grpc服务
     * 调用登录接口，获取token
     * 初始化交易标的
     */
    private TwoArbitrage(){
        ManagedChannel mgChannel = ManagedChannelBuilder.forAddress(host, mgport).usePlaintext().build();
        //初始化行情网关
        mgStub = MarketDataManageGrpc.newStub(mgChannel);
        ManagedChannel tgChannel = ManagedChannelBuilder.forAddress(host, tgport).usePlaintext().build();
        //登录获取token
        LoginGrpc.LoginBlockingStub loginBlockingStub = LoginGrpc.newBlockingStub(tgChannel);
        LoginOuterClass.RspLogin rspLogin = loginBlockingStub.login(LoginOuterClass.ReqLogin.newBuilder().setUsername(username).setPassword(password).build());
        String token = rspLogin.getToken();
        //初始化交易网关和查询网关
        tgStub = OrderReportingGrpc.newStub(tgChannel).withCallCredentials(new JwtCallCredential(token));
        queryStub = QueryServiceGrpc.newStub(tgChannel).withCallCredentials(new JwtCallCredential(token));
        queryBlockingStub = QueryServiceGrpc.newBlockingStub(tgChannel).withCallCredentials(new JwtCallCredential(token));

        for (String exchange : exchanges) {
            Common.Symbol symbol = Common.Symbol.newBuilder().setBaseCurrency(baseCurrency).setQuoteCurrency(quoteCurrency).setExchangeType(exchange).build();
            symbols.add(symbol);
        }

        //查询初始仓位
        Orderreporting.SubaccountResponse subaccountResponse = queryBlockingStub.getSubaccounts(Orderreporting.SubaccountRequest.newBuilder().build());
        for (Orderreporting.Subaccount subaccount : subaccountResponse.getDataList())
        {
           String exchange = subaccount.getExchangeType();
           if (exchanges.contains(exchange)) {
               Map<String, BigDecimal> exchangeBalanceMap = new HashMap<>();
               for (Orderreporting.AssetBalance assetBalance : subaccount.getAssetBalanceList()) {
                   if (assetBalance.getAsset().equals(baseCurrency) || assetBalance.getAsset().equals(quoteCurrency)) {
                       exchangeBalanceMap.putIfAbsent(assetBalance.getAsset(), new BigDecimal(assetBalance.getFree()));
                   }
               }
               if (!exchangeBalanceMap.containsKey(baseCurrency)) {
                   exchangeBalanceMap.put(baseCurrency, BigDecimal.ZERO);
               }
               if (!exchangeBalanceMap.containsKey(quoteCurrency)) {
                   exchangeBalanceMap.put(quoteCurrency, BigDecimal.ZERO);
               }
               lastCurrencyMap.put(exchange, exchangeBalanceMap);
           }
        }

        for (String exchange : exchanges) {
            bidAsk1Map.put(exchange, null);
        }

        for (int i = 0; i< exchanges.size(); i++) {
            for (int j = i + 1; j< exchanges.size(); j++) {
                String exchangePair = exchanges.get(i) + "/" + exchanges.get(j);
                arbitrageSymbolMap.put(exchangePair, new ArbitrageSymbol(exchanges.get(i), exchanges.get(j)));
            }
        }

        initRecord();
        subTrade();
        subMarketData();
    }
    /**
     * 订阅成交回报，在回报时维护orderMap,将已成交订单移除
     */
    private void subTrade(){
        //订阅成交回报
        StreamObserver<Orderreporting.ReqSubTrade> reqSubTradeStreamObserver = tgStub.subTrade(new StreamObserver<Orderreporting.RtnSubTrade>() {
            @Override
            public void onNext(Orderreporting.RtnSubTrade rtnSubTrade) {
                String exchange = rtnSubTrade.getTrade().getSymbol().getExchangeType();
                String price = rtnSubTrade.getTrade().getPrice();
                String commission = rtnSubTrade.getTrade().getCommission();
                //更新btc余额，这里只考虑全部成交
                if (new BigDecimal(rtnSubTrade.getTrade().getQuantity()).compareTo(new BigDecimal(quantity)) == 0) {
                    if (rtnSubTrade.getTrade().getSide().equals("buy")) {
                        System.out.println("买单已成交");
                        Map<String, BigDecimal> balanceMap = lastCurrencyMap.get(exchange);
                        BigDecimal baseBalance = balanceMap.get(baseCurrency);
                        BigDecimal quoteBalance = balanceMap.get(quoteCurrency);
                        balanceMap.put(baseCurrency, baseBalance.add(new BigDecimal(quantity).subtract(new BigDecimal(commission))));
                        balanceMap.put(quoteCurrency, quoteBalance.subtract(new BigDecimal(quantity).multiply(new BigDecimal(price))));
                        orderMap.remove("buy");
                    } else if (rtnSubTrade.getTrade().getSide().equals("sell")) {
                        System.out.println("卖单已成交");
                        Map<String, BigDecimal> balanceMap = lastCurrencyMap.get(exchange);
                        BigDecimal baseBalance = balanceMap.get(baseCurrency);
                        BigDecimal quoteBalance = balanceMap.get(quoteCurrency);
                        balanceMap.put(baseCurrency, baseBalance.subtract(new BigDecimal(quantity)));
                        balanceMap.put(quoteCurrency, quoteBalance.add(new BigDecimal(quantity).multiply(new BigDecimal(price)).subtract(new BigDecimal(commission))));
                        orderMap.remove("sell");
                    }
                }
            }

            @Override
            public void onError(Throwable t) {
                t.printStackTrace();
            }

            @Override
            public void onCompleted() {

            }
        });
        //不要漏掉下面这句调用，向服务请求订阅成交回报
        reqSubTradeStreamObserver.onNext(Orderreporting.ReqSubTrade.newBuilder().build());
        System.out.println("订阅了所有交易所的成交回报" );

    }

    /**
     * 订阅行情，比较不同交易所的ask1与bid1，
     */
    private void subMarketData(){
        StreamObserver<MarketGateway.ReqSubDepth> requestObserver = mgStub.subDepth(new StreamObserver<MarketGateway.DepthsDataResponse>() {
            @Override
            public void onNext(MarketGateway.DepthsDataResponse depthsDataResponse) {
                if (!depthsDataResponse.getCRsp().getIsSuccess()){
                    return;
                }
                MarketGateway.DepthsData depthsData = depthsDataResponse.getData();
                String exchange = depthsData.getSymbol().getExchangeType();
                BidAsk1 bidAsk1 = new BidAsk1(depthsData.getSymbol().getExchangeType(), new BigDecimal(depthsData.getBids(0).getPrice()),
                        new BigDecimal(depthsData.getBids(0).getQuantity()), new BigDecimal(depthsData.getAsks(0).getPrice()),
                        new BigDecimal(depthsData.getAsks(0).getQuantity()), depthsData.getTimestamp());
                System.out.println(bidAsk1);
                bidAsk1Map.put(exchange, bidAsk1);
                for (Map.Entry<String, ArbitrageSymbol> entry : arbitrageSymbolMap.entrySet()) {
                    if (entry.getKey().contains(exchange)) {
                        if(!bidAsk1.equals(entry.getValue().getBaseBidAsk1()) || !bidAsk1.equals(entry.getValue().getQuoteBidAsk1())) {
                            entry.getValue().update(exchange, bidAsk1);
//                            record();
                        }
                    }
                }

                handleMd();
            }

            @Override
            public void onError(Throwable t) {
                t.printStackTrace();
            }

            @Override
            public void onCompleted() {

            }
        });
        for (Common.Symbol symbol : symbols) {
            System.out.println("订阅了" + symbol.getExchangeType() + "交易所的行情" );
            MarketGateway.ReqSubDepth reqSubDepth = MarketGateway.ReqSubDepth.newBuilder().setSymbol(symbol).build();
            requestObserver.onNext(reqSubDepth);
        }
    }

    /**
     * 初始化行情记录的数据结构
     */
    private void initRecord() {
        List<String> l = new ArrayList<>();
        for (Map.Entry<String, ArbitrageSymbol> entry : arbitrageSymbolMap.entrySet()) {
            ArbitrageSymbol arbitrageSymbol = entry.getValue();
            l.add(arbitrageSymbol.getBaseExchange() + " / " + arbitrageSymbol.getQuoteExchange());
            l.add(arbitrageSymbol.getQuoteExchange() + " / " + arbitrageSymbol.getBaseExchange());
        }
        records.add(l);
    }

    /**
     * 初始化行情记录的数据结构
     */
    private void record() {
        List<String> l = new ArrayList<>();
        for (Map.Entry<String, ArbitrageSymbol> entry : arbitrageSymbolMap.entrySet()) {
            ArbitrageSymbol arbitrageSymbol = entry.getValue();
            l.add(arbitrageSymbol.getBaseQuoteSpread().setScale(5, RoundingMode.HALF_EVEN).toString());
            l.add(arbitrageSymbol.getQuoteBaseSpread().setScale(5, RoundingMode.HALF_EVEN).toString());
        }
        records.add(l);
    }


    /**
     * 处理最新的bid1 ask1的数据，决定是否要进行交易
     */
    private void handleMd() {
        for (Map.Entry<String, ArbitrageSymbol> entry : arbitrageSymbolMap.entrySet()) {
            if (entry.getValue() != null)
            {
                System.out.println(entry.getValue());
            }
        }
    }
//
//    /**
//     * 调用接口进行限价买入，收到响应后将orderId放入orderMap中
//     */
//    private void buy(){
//        String price = lastPrice.setScale(2, RoundingMode.HALF_EVEN).toString();
//        final Orderreporting.InsertOrderRequest orderTradeRequest =Orderreporting.InsertOrderRequest.newBuilder().
//                setSubaccountCode(subaccountCode).setSymbol(btcusdtSymbol).setQuantity(quantity).setPrice(price).setType("limit").setSide("buy").build();
//        System.out.println("报单买入： 价格" + price);
//        orderMap.put("buy", "placeHolderOrderId");//为避免发起报单到收到响应的过程中行情会再次出发报单，先在map里放一个占位符
//        tgStub.insertOrder(orderTradeRequest, new StreamObserver<Orderreporting.InsertOrderResponse>() {
//            @Override
//            public void onNext(Orderreporting.InsertOrderResponse value) {
//                if(!value.getCRsp().getIsSuccess()){
//                    System.out.println("报单失败: " + value.getCRsp().getErrMsg());
//                    orderMap.remove("buy");
//                }else{
//                    //收到响应后更新orderId
//                    orderMap.put("buy", value.getOrderId());
//                    System.out.println("报单成功：" + value.getOrderId());
//                }
//            }
//
//            @Override
//            public void onError(Throwable t) {
//                System.out.println("报单失败: " + t.getMessage());
//                orderMap.remove("buy");
//            }
//
//            @Override
//            public void onCompleted() {
//
//            }
//        });
//    }
//
//    /**
//     * 调用接口进行限价卖出，收到响应后将orderId放入orderMap中
//     */
//    private void sell(){
//        String price = lastPrice.setScale(2, RoundingMode.HALF_EVEN).toString();
//        final Orderreporting.InsertOrderRequest orderTradeRequest =Orderreporting.InsertOrderRequest.newBuilder().
//                setSubaccountCode(subaccountCode).setSymbol(btcusdtSymbol).setQuantity(quantity).setPrice(price).setType("limit").setSide("sell").build();
//        System.out.println("报单卖出： 价格" + price);
//        orderMap.put("sell", "placeHolderOrderId");//为避免发起报单到收到响应的过程中行情会再次出发报单，先在map里放一个占位符
//        tgStub.insertOrder(orderTradeRequest, new StreamObserver<Orderreporting.InsertOrderResponse>() {
//            @Override
//            public void onNext(Orderreporting.InsertOrderResponse value) {
//                if(!value.getCRsp().getIsSuccess()){
//                    System.out.println("报单失败: " + value.getCRsp().getErrMsg());
//                    orderMap.remove("sell");
//                }else{
//                    //收到响应后更新orderId
//                    orderMap.put("sell", value.getOrderId());
//                    System.out.println("报单成功：" + value.getOrderId());
//                }
//            }
//
//            @Override
//            public void onError(Throwable t) {
//                System.out.println("报单失败: " + t.getMessage());
//                orderMap.remove("sell");
//            }
//
//            @Override
//            public void onCompleted() {
//
//            }
//        });
//    }
//
//    /**
//     * 撤单，收到响应后，将订单从orderMap中移除
//     * @param orderId 订单ID
//     */
//    private void cancel(String orderId) {
//        if(orderId.equals("placeHolderOrderId")) //报单还未受理，不需要撤单
//            return;
//        final Orderreporting.CancelOrderRequest cancelOrderRequest = Orderreporting.CancelOrderRequest.newBuilder().setOrderId(orderId).build();
//        tgStub.cancelOrder(cancelOrderRequest, new StreamObserver<Orderreporting.CancelOrderResponse>() {
//            @Override
//            public void onNext(Orderreporting.CancelOrderResponse value) {
//                if(value.getCRsp().getIsSuccess()){
//                    if(value.getData().getOrderId().equals(orderMap.get("buy"))){
//                        System.out.println("取消买单成功");
//                        orderMap.remove("buy");
//                    }else if(value.getData().getOrderId().equals(orderMap.get("sell"))){
//                        System.out.println("取消卖单成功");
//                        orderMap.remove("sell");
//                    }
//                }else{
//                    System.out.println("撤单失败： " + value.getCRsp().getErrMsg());
//                }
//            }
//
//            @Override
//            public void onError(Throwable t) {
//                t.printStackTrace();
//            }
//
//            @Override
//            public void onCompleted() {
//
//            }
//        });
//    }
//

//
//    /**
//     * 判断移动平均线是否交叉
//     * 0 未交叉
//     * -1 短周期下穿长周期
//     * 1 短周期上穿长周期
//     * @return 移动平均线是否交叉
//     */
//    private int checkPriceDiff() {
//        int result = 0;
//        BigDecimal longTimeMaValue = longTimeMa.getAvg();
//        BigDecimal shortTimeMaValue = shortTimeMa.getAvg();
//        if(longTimeMaValue.compareTo(BigDecimal.ZERO) > 0 && shortTimeMaValue.compareTo(BigDecimal.ZERO) > 0){
//            BigDecimal newPriceDiff = shortTimeMaValue.subtract(longTimeMaValue);
//            if(priceDiff != null){
//                System.out.println("previous price diff : " + priceDiff + ", latest price diff: " + newPriceDiff + ", shortPeriod: " + shortTimeMaValue + ", longPeriod" + longTimeMaValue);
//                if(priceDiff.compareTo(BigDecimal.ZERO)<0 && newPriceDiff.compareTo(BigDecimal.ZERO) > 0){
//                    result = 1;
//                }else if (priceDiff.compareTo(BigDecimal.ZERO)>0 && newPriceDiff.compareTo(BigDecimal.ZERO) < 0){
//                    result = -1;
//                }
//            }
//            priceDiff = newPriceDiff;
//        }
//        return result;
//    }
//
//    /**
//     * 0 无仓， 1买入等待成交， 2 多仓， 3 卖出等待成交
//     * -1异常
//     * @return 策略状态
//     */
//    private int getState(){
//        if(btcBalance.compareTo(initBtcBalance) > 0 ){
//            //多仓
//            if(orderMap.get("buy") != null){
//                System.out.println("多仓状态有买单，异常退出");
//                System.exit(0);
//            }
//            if(orderMap.get("sell") !=null){
//                return 3;
//            } else{
//                return 2;
//            }
//        } else if (btcBalance.compareTo(initBtcBalance) == 0 ){
//            //无仓
//            if(orderMap.get("sell") != null){
//                System.out.println("无仓状态有卖单，异常退出");
//                System.exit(0);
//            }
//            if(orderMap.get("buy") !=null){
//                return 1;
//            }else{
//                return 0;
//            }
//        } else {
//            //空仓，本例不做空
//            System.out.println("btc余额["+btcBalance.toString()+"]小于初始值["+initBtcBalance+"]，异常退出");
//            System.exit(0);
//        }
//        return -1;
//    }

    public static void Array2CSV(List<List<String>> data, String path)
    {
        try {
            BufferedWriter out =new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path),"UTF-8"));
            for (int i = 0; i < data.size(); i++)
            {
                List<String> onerow=data.get(i);
                for (int j = 0; j < onerow.size(); j++)
                {
                    out.write(onerow.get(j));
                    out.write(",");
                }
                out.newLine();
            }
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
